from django.shortcuts import render


def index(request):
    return render(request, 'index.html')


def manage(request):
    return render(request, 'manage.html')
